#! /usr/bin/python3
# setup.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Setup script for Shit"""

import os
import setuptools

with open("README.md", "r") as fh:
    LONG_DESCRIPTION = fh.read()

if os.name != 'nt':
    SCRIPTS = [
        "scripts/shit-desktop-install",
        "scripts/shit-desktop-uninstall"
    ]
else:
    SCRIPTS = []

setuptools.setup(
    name="shit",
    version="0.0.1",
    author="Alan Davies",
    author_email="alan.davies@gmail.com",
    license="GPL3",
    description="Shit is a git log viewer",
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/cs96and/shit",
    python_requires='>=3',
    packages=setuptools.find_packages(),
    package_data={
        'shit': ['icons/*'],
    },
    entry_points={
        'gui_scripts': [
            'shit = shit:main',
        ],
    },
    scripts=SCRIPTS,
    install_requires=[
        'PyQt6>=6.0',
        'pygit2>=1.11.0'
    ],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3 :: Only"
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Environment :: Win32 (MS Windows)",
        "Environment :: X11 Applications :: Qt",
        "Intended Audience :: Developers"
        "Topic :: Software Development :: Version Control :: Git",
        "Natural Language :: English"
    ]
)
