#! /usr/bin/python3
# shit_app.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Main application and GUI window for shit"""

import hashlib
import os
import re
import sys
import tempfile
import urllib.request

import pygit2
from PyQt6.QtCore import Qt, QModelIndex, QProcess, QTimer, pyqtSlot
from PyQt6.QtGui import QFont, QIcon, QFontDatabase, QImage, QPalette, QPixmap
from PyQt6.QtWidgets import (QApplication, QMessageBox, QTextEdit, QLineEdit, QMainWindow, QSplitter, QLabel,
                             QFileIconProvider, QHBoxLayout, QVBoxLayout, QWidget, QComboBox)

import shit
from .background_task import create_background_task
from .detachable_process import DetachableProcess
from .garbage_collector import GarbageCollector
from .ui.git_commit_view import GitCommitView
from .ui.git_log_view import GitLogView
from .ui.greyable_icon import GreyableIcon


class ShitMainWindow(QMainWindow):
    """Wrapper around QMainWindow to handle F5 being pressed"""

    def __init__(self, log_table):
        """ShitMainWindow constructor"""
        super().__init__()
        self.log_table = log_table

    def keyPressEvent(self, event):
        """Handle a key being pressed"""
        if event.key() == Qt.Key.Key_F5:
            self.log_table.refresh()
        else:
            super().keyPressEvent(event)


class ShitApp(QApplication):
    """Shit main application class"""

    def __init__(self, argv):
        """ShitApp constructor"""
        super().__init__(argv)

        self.gc = GarbageCollector(self)

        # QT6 doesn't seem to set the placeholder text color correctly for dark themes.
        # Set the placeholder text color to be the same as normal text, but with reduced alpha.
        palette = self.palette()
        placeholder_color = palette.color(QPalette.ColorRole.Text)
        placeholder_color.setAlpha(128)
        palette.setColor(QPalette.ColorRole.PlaceholderText, placeholder_color)
        self.setPalette(palette)

        self.setApplicationName("shit")
        self.setApplicationDisplayName("shit")
        self.setWindowIcon(QIcon(os.path.join(shit.ICONS_DIR, "poop.png")))
        self.icon_provider = QFileIconProvider() if os.name == 'nt' else None

        is_file = False

        if len(argv) > 1:
            try:
                if os.path.isdir(argv[1]):
                    os.chdir(argv[1])
                elif os.path.isfile(argv[1]):
                    is_file = True
                    dirname = os.path.dirname(argv[1])
                    if dirname:
                        os.chdir(dirname)
                else:
                    raise RuntimeError(f"{argv[1]} is neither a file or directory")
            except BaseException as error:
                QMessageBox.critical(None, "shit", str(error))
                sys.exit(1)

        repo_dir = pygit2.discover_repository(os.getcwd())

        if repo_dir is None:
            QMessageBox.critical(None, "shit", f"{os.getcwd()} is not a git repository")
            sys.exit(1)

        self.repo = pygit2.Repository(repo_dir)
        self.path = os.path.relpath(os.getcwd(), self.repo.workdir)

        if is_file:
            if self.path == '.':
                self.path = os.path.basename(argv[1])
            else:
                self.path = os.path.join(self.path, os.path.basename(argv[1]))

        self.__init_ui()
        self.child_processes = {}
        self.pixmap_cache = {}

        QTimer.singleShot(0, lambda: self.on_branch_changed(self.branch_list.currentText()))

    def __enter__(self):
        return self

    def __exit__(self, _exception_type, _exception_value, _traceback):
        for process in self.child_processes:
            process.detach()

    def __init_ui(self):
        """Initialize the UI widgets"""
        # Use a timer to detect end of typing into the search box
        self.typing_timer = QTimer(singleShot=True, timeout=self.on_typing_timeout)

        self.search_box = QLineEdit(placeholderText="Enter regex to search messages, authors, dates, SHA-1...",
                                    textEdited=lambda: self.typing_timer.start(300),
                                    returnPressed=lambda: self.typing_timer.setInterval(0))

        self.regex_icon = GreyableIcon(os.path.join(shit.ICONS_DIR, "asterisk.png"))
        toggle_regex_action = self.search_box.addAction(self.regex_icon.enabled(), QLineEdit.ActionPosition.LeadingPosition)
        toggle_regex_action.setCheckable(True)
        toggle_regex_action.setChecked(True)
        toggle_regex_action.setText("Toggle regex mode")
        toggle_regex_action.toggled.connect(self.on_regex_toggled)

        clear_icon = QIcon(os.path.join(shit.ICONS_DIR, "cross.png"))
        clear_action = self.search_box.addAction(clear_icon, QLineEdit.ActionPosition.TrailingPosition)
        clear_action.setText("Clear")
        clear_action.triggered.connect(lambda: [self.search_box.clear(), self.on_typing_timeout()])

        self.branch_list = self.__get_branch_list()
        self.branch_list.currentTextChanged.connect(self.on_branch_changed)

        top_layout = QHBoxLayout()
        top_layout.addWidget(self.search_box)
        top_layout.addWidget(QLabel("Branch:"))
        top_layout.addWidget(self.branch_list)
        top_layout.setStretch(0, 1)
        top_layout.setStretch(1, 0)
        top_layout.setStretch(2, 0)
        top_layout.setContentsMargins(0, 2, 0, 2)

        top_widget = QWidget()
        top_widget.setLayout(top_layout)

        splitter = QSplitter(Qt.Orientation.Vertical)
        splitter.setContentsMargins(0, 0, 0, 0)

        # Create the git log table
        self.log_table = GitLogView(self.repo, self.path, stretched_column=1)
        self.log_table.populate_finished.connect(self.on_log_populate_finished)
        self.log_table.commits_selected.connect(self.on_commits_selected)
        self.log_table.invalid_selection.connect(self.on_invalid_commit_selection)
        splitter.addWidget(self.log_table)

        # Create the message window
        self.message_window = QTextEdit(textInteractionFlags=Qt.TextInteractionFlag.TextBrowserInteraction)
        message_font = self.__get_log_font(self.message_window.font().pointSize())
        self.message_window.setFont(message_font)

        # Create the gravatar panel
        self.gravatar_panel = QLabel()
        self.gravatar_panel.setMinimumSize(80, 0)

        message_and_gravatar_layout = QHBoxLayout()
        message_and_gravatar_layout.addWidget(self.message_window)
        message_and_gravatar_layout.addWidget(self.gravatar_panel)
        message_and_gravatar_layout.setContentsMargins(0, 0, 0, 0)
        message_and_gravatar_layout.setStretch(0, 1)
        message_and_gravatar_layout.setStretch(1, 0)

        message_and_gravatar_widget = QWidget()
        message_and_gravatar_widget.setLayout(message_and_gravatar_layout)

        splitter.addWidget(message_and_gravatar_widget)

        self.commit_table = GitCommitView(self.repo, self.path, self.icon_provider, 1)
        self.commit_table.activated.connect(self.on_file_activated)
        splitter.addWidget(self.commit_table)

        main_layout = QVBoxLayout()
        main_layout.setContentsMargins(6, 6, 6, 6)
        main_layout.addWidget(top_widget)
        main_layout.addWidget(splitter)

        # Create the main window and set the main layout
        self.root = ShitMainWindow(self.log_table)
        self.root.resize(1000, 800)
        self.root.setWindowTitle(os.getcwd())

        central_widget = QWidget()
        central_widget.setLayout(main_layout)
        self.root.setCentralWidget(central_widget)

        splitter.setSizes([330, 150, 300])
        splitter.setStretchFactor(0, 1)
        splitter.setStretchFactor(1, 0)
        splitter.setStretchFactor(2, 1)

        # Add the welcome message to the log window
        self.root.show()

    def __get_log_font(self, default_size):
        """Return log message font from settings"""
        font_name = shit.SETTINGS["logfont"]
        if font_name:
            font = QFont(font_name)
        else:
            font = QFontDatabase.systemFont(QFontDatabase.SystemFont.FixedFont)

        font_size = shit.SETTINGS["logfontsize"]
        if font_size:
            font.setPointSizeF(font_size)
        else:
            font.setPointSizeF(default_size)

        return font

    def __get_branch_list(self):
        """Return a QComboBox populated with the list of all branches in the repo"""
        branch_list = QComboBox(insertPolicy=QComboBox.InsertPolicy.InsertAtBottom)

        split_regex = re.compile(r"(\d+)")
        natsort = lambda str: tuple(int(part) if part.isdigit() else part.casefold() for part in re.split(split_regex, str))

        # Add local branches, naturally sorted.
        for branch in sorted(self.repo.branches.local, key=natsort):
            branch_list.addItem(str(branch))

        branch_list.insertSeparator(branch_list.count())

        # Add remote branches, naturally sorted.
        for branch in sorted(self.repo.branches.remote, key=natsort):
            branch_list.addItem(str(branch))

        branch_list.insertSeparator(branch_list.count())

        # Add stashes
        i = 0
        while True:
            stash_name = f"stash@{{{i}}}"
            try:
                revision = self.repo.revparse_single(stash_name)
            except KeyError:
                break
            else:
                branch_list.addItem(stash_name)
                branch_list.setItemData(branch_list.count() - 1, revision.message.rstrip(), Qt.ItemDataRole.ToolTipRole)
                i += 1

        # Set the current index of the combo box to the current branch
        index = branch_list.findText(self.repo.head.shorthand)
        if index >= 0:
            branch_list.setCurrentIndex(index)
        else:
            # Probably in a detached head state.  Add the short commit id as a separate entry at the top of the list.
            branch_list.insertItem(0, str(self.repo.head.peel().short_id))
            branch_list.setCurrentIndex(0)

        return branch_list

    def __get_email_for_commit(self, commit):
        return commit.author.email if commit is not None else self.repo.config["user.email"]

    def __display_gravatar_pixmap(self, commit):
        """Display the gravatar for the author of a commit"""
        pixmap = None
        email = self.__get_email_for_commit(commit)
        if email is not None:
            try:
                pixmap = self.pixmap_cache[email]
            except KeyError:
                self.pixmap_cache[email] = None
                create_background_task(lambda: self.__fetch_gravatar_pixmap(email),
                                       self.on_gravatar_image,
                                       lambda error: print(f"Failed to fetch gravatar image for {email}, {error}"))

        if pixmap is not None:
            self.gravatar_panel.setPixmap(pixmap)
        else:
            self.gravatar_panel.clear()

    def show_commit_message(self, commit):
        """Show the git commit message for a given commit"""
        self.message_window.clear()
        self.__display_gravatar_pixmap(commit)

        if commit is None:
            self.message_window.append(f"SHA-1: {pygit2.Oid(b'')}\n")
            self.message_window.append("Working directory changes")
        else:
            self.message_window.append(f"SHA-1: {commit.id}\n")
            split_message = commit.message.rstrip("\n").split('\n', 1)
            self.message_window.setFontWeight(QFont.Weight.Bold)
            self.message_window.append('* ' + split_message[0])
            self.message_window.setFontWeight(QFont.Weight.Normal)
            if len(split_message) > 1:
                self.message_window.append(split_message[1])

            # Append the notes if there are any
            try:
                note = self.repo.lookup_note(str(commit.id))
                self.message_window.setFontWeight(QFont.Weight.Bold)
                self.message_window.append("\nNotes:")
                self.message_window.setFontWeight(QFont.Weight.Normal)
                self.message_window.append(note.message)
            except KeyError:
                pass

        vertical_scrollbar = self.message_window.verticalScrollBar()
        vertical_scrollbar.setValue(vertical_scrollbar.minimum())

    def on_gravatar_image(self, gravatar_email, pixmap):
        """Handle receipt of gravatar image"""
        self.pixmap_cache[gravatar_email] = pixmap

        selected_commit = self.log_table.selected_commit()
        selected_email = self.__get_email_for_commit(selected_commit)
        if selected_email == gravatar_email:
            self.gravatar_panel.setPixmap(pixmap)

    @pyqtSlot(bool)
    def on_regex_toggled(self, is_enabled):
        """Handle enabling/disabling of regex mode"""
        action = self.sender()
        if is_enabled:
            action.setIcon(self.regex_icon.enabled())
        else:
            action.setIcon(self.regex_icon.disabled())

        if self.search_box.text():
            self.on_typing_timeout()

    @pyqtSlot()
    def on_typing_timeout(self):
        """Handle the search terms changing"""
        if self.search_box.actions()[0].isChecked():
            self.log_table.model().setFilterRegularExpression(self.search_box.text())
        else:
            self.log_table.model().setFilterFixedString(self.search_box.text())

    @pyqtSlot(str)
    def on_branch_changed(self, new_branch):
        """Handle a new branch being selected from the drop down list"""
        self.branch_list.repaint()
        self.commit_table.on_branch_changed(new_branch)
        self.log_table.on_branch_changed(new_branch)

    @pyqtSlot(str)
    def on_log_populate_finished(self, branch_name):
        """Handle population of the commit log finishing"""
        window_path = self.path if self.path != '.' else os.getcwd()
        self.root.setWindowTitle(f"{window_path} [{branch_name}]")
        self.log_table.selectRow(1)
        self.commit_table.make_stretched_column_resizable()

    @pyqtSlot(object, object)
    def on_commits_selected(self, commit, prev_commit):
        """Handle commits being selected from the log view"""
        # The diff calculation may take a while, so update the view to show the new selection
        self.sender().parent().repaint()

        self.show_commit_message(commit)
        self.commit_table.populate(commit=commit, prev_commit=prev_commit)

    @pyqtSlot()
    def on_invalid_commit_selection(self):
        """Handle no commits, or > 2 commits being selected from the log view"""
        self.message_window.clear()
        self.gravatar_panel.clear()
        self.commit_table.model().remove_all_rows()

    @pyqtSlot(QModelIndex)
    def on_file_activated(self, _model_index):
        """Handle a file being activated (double clicked or enter pressed)"""

        temp_dir = os.path.join(tempfile.gettempdir(), "shit")
        try:
            os.mkdir(temp_dir)
        except FileExistsError:
            pass

        # Multiple files may be selected, so use the selection model, rather than the model_index parameter
        for model_index in self.sender().selectionModel().selectedRows(0):
            data = model_index.data(Qt.ItemDataRole.UserRole)
            if not data:
                continue

            patch = data['patch']

            old_tmp = [0, '']   # [ file handle, path ]
            new_tmp = old_tmp.copy()
            left_file = None
            right_file = None

            diff_args = shit.SETTINGS["diffargs"].split()

            if '{left}' in diff_args:
                old_id = data['old_id']
                old_name, old_ext = os.path.splitext(os.path.basename(patch.delta.old_file.path))

                old_tmp = tempfile.mkstemp(dir=temp_dir, prefix=f"{old_name}.{str(old_id):.6}.", suffix=old_ext)
                with open(old_tmp[0], 'wb') as old_temp_file:
                    old_blob_id = patch.delta.old_file.id
                    if old_blob_id != pygit2.Oid(b''):
                        old_temp_file.write(self.repo[old_blob_id].data)
                left_file = old_tmp[1]

            if '{right}' in diff_args:
                new_id = data['new_id']
                if new_id is None:
                    # Use working dir file
                    right_file = os.path.join(self.repo.workdir, patch.delta.new_file.path)
                    if not os.path.exists(right_file):
                        # Working dir file has been deleted, use a blank temp file
                        right_file = None

                if right_file is None:
                    new_name, new_ext = os.path.splitext(os.path.basename(patch.delta.new_file.path))
                    new_tmp = tempfile.mkstemp(dir=temp_dir, prefix=f"{new_name}.{str(new_id):.6}.", suffix=new_ext)
                    with open(new_tmp[0], 'wb') as new_temp_file:
                        new_blob_id = patch.delta.new_file.id
                        if new_blob_id != pygit2.Oid(b''):
                            new_temp_file.write(self.repo[new_blob_id].data)
                    right_file = new_tmp[1]

            diff_args = (i.format(left=left_file, right=right_file, commit=str(data['new_id']),
                                  file=self.repo.workdir + patch.delta.old_file.path)
                         for i in diff_args)

            proc = DetachableProcess(finished=self.on_process_exit)
            proc.start(shit.SETTINGS["diff"], diff_args)
            self.child_processes[proc] = [old_tmp[1], new_tmp[1]]

    @pyqtSlot(int, QProcess.ExitStatus)
    def on_process_exit(self):
        """Handle diff process exiting.  Delete the temporary files"""
        data = self.child_processes[self.sender()]
        if data[0] != '':
            os.remove(data[0])
        if data[1] != '':
            os.remove(data[1])
        del self.child_processes[self.sender()]

    @staticmethod
    def __fetch_gravatar_pixmap(email):
        stripped_email = email.encode("utf-8").strip().lower()
        email_hash = hashlib.md5(stripped_email).hexdigest()
        response = urllib.request.urlopen(f"https://www.gravatar.com/avatar/{email_hash}?d=robohash")
        img = QImage.fromData(response.read())
        return (email, QPixmap.fromImage(img))
