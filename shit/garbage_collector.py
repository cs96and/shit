#! /usr/bin/python3
# garbage_collector.py
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import gc
from PyQt6.QtCore import QObject, QTimer


class GarbageCollector(QObject):
    """
    https://www.riverbankcomputing.com/pipermail/pyqt/2011-August/030378.html
    Disable automatic garbage collection and instead collect manually
    every INTERVAL milliseconds.

    This is done to ensure that garbage collection only happens in the GUI
    thread, as otherwise Qt can crash.
    """

    INTERVAL = 5000

    def __init__(self, parent, debug=False):
        QObject.__init__(self, parent)
        if (debug):
            gc.set_debug(gc.DEBUG_STATS)

        self.timer = QTimer(self)
        self.timer.timeout.connect(lambda: gc.collect())

        gc.disable()
        self.timer.start(self.INTERVAL)
