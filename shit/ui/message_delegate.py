#! /usr/bin/python3
# message_delegate.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""QT Delegate for drawing the git log message along with any branch/tag markers"""

import pygit2
from PyQt6.QtCore import Qt, QEvent, QRectF
from PyQt6.QtGui import QBrush, QColor, QPainter, QPalette, QPen
from PyQt6.QtWidgets import QStyle, QStyledItemDelegate

__all__ = ["MessageDelegate"]


class Tag(str):
    """Typed wrapper around string for tags"""
    pass


class Branch(str):
    """Typed wrapper around string for branches"""
    pass


class MessageDelegate(QStyledItemDelegate):
    """Delegate to draw the log message of a commit along with any branch/tag markers"""

    HORIZONTAL_PADDING = 2.5
    REF_GAP = 4.0
    REMOTE_BRANCH_COLOUR = QColor(0xFF, 0xD1, 0x80)

    def __init__(self, repo, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.repo = repo
        self.refresh_refs()

    def refresh_refs(self):
        """Refresh the cached references to tags and branches"""
        self.ref_map = {}
        for ref in (self.repo.lookup_reference(ref) for ref in self.repo.references):
            if ref.type == pygit2.GIT_REF_OID:
                RefType = Tag if ref.name.startswith("refs/tags/") else Branch
                ref_list = self.ref_map.setdefault(ref.peel().id, [])
                ref_list.append(RefType(ref.shorthand))

    def paint(self, painter, style_option, model_index):
        """Draw the log message of a commit along with any branches/tag markers"""
        self.initStyleOption(style_option, model_index)
        painter.setRenderHint(QPainter.RenderHint.Antialiasing)

        commit_id = model_index.siblingAtColumn(0).data(Qt.ItemDataRole.UserRole)
        rect = QRectF(style_option.rect)

        if style_option.state & QStyle.StateFlag.State_Active:
            colour_group = QPalette.ColorGroup.Active
        else:
            colour_group = QPalette.ColorGroup.Inactive

        if style_option.state & QStyle.StateFlag.State_Selected:
            default_text_colour = style_option.palette.color(colour_group, QPalette.ColorRole.HighlightedText)
            highlight_colour = style_option.palette.color(colour_group, QPalette.ColorRole.Highlight)
            painter.fillRect(rect, highlight_colour)
        else:
            default_text_colour = style_option.palette.color(colour_group, QPalette.ColorRole.Text)

        pen = QPen()
        brush = QBrush(Qt.BrushStyle.SolidPattern)
        rect.setX(rect.x() + self.HORIZONTAL_PADDING)

        def __set_pen_colour(colour):
            pen.setColor(colour)
            painter.setPen(pen)

        def __reset_pen():
            __set_pen_colour(default_text_colour)

        ref_list = self.ref_map.get(commit_id, [])

        for ref in ref_list:
            if "/" in ref:
                brush.setColor(QColor("#00b159"))
            elif isinstance(ref, Tag):
                brush.setColor(QColor("#fdf470"))
            elif self.repo.head.shorthand == ref:
                brush.setColor(QColor("#ee4035"))
            else:
                brush.setColor(QColor("#ff77aa"))

            # Need to make sure pen is set before we calculate the bounding rect, else it will sometimes
            # calculate a zero-sized box
            __set_pen_colour(style_option.palette.color(QPalette.ColorRole.Text))

            # Calculate the bounding rectangle for the ref text and add some horizontal padding around the text
            sub_rect = painter.boundingRect(rect, Qt.AlignmentFlag.AlignLeft | Qt.AlignmentFlag.AlignVCenter, ref)
            sub_rect.setRight(sub_rect.right() + (self.HORIZONTAL_PADDING * 2))

            painter.setBrush(brush)
            painter.drawRoundedRect(sub_rect, 2, 2)

            # Draw the ref name in the rectangle (offset to cope with the horizontal padding we added)
            __set_pen_colour(Qt.GlobalColor.black)
            painter.drawText(sub_rect.translated(self.HORIZONTAL_PADDING, 0),
                             Qt.AlignmentFlag.AlignLeft | Qt.AlignmentFlag.AlignVCenter, ref)

            rect.setX(rect.x() + sub_rect.width() + self.REF_GAP)

        __reset_pen()
        painter.setBrush(QBrush())
        painter.drawText(rect, Qt.AlignmentFlag.AlignVCenter, model_index.data().split("\n", 1)[0])

    def sizeHint(self, style_option, model_index):
        """Calculate the size of the message column"""
        self.initStyleOption(style_option, model_index)

        commit_id = model_index.siblingAtColumn(0).data(Qt.ItemDataRole.UserRole)
        ref_list = self.ref_map.get(commit_id, [])

        size = style_option.fontMetrics.size(Qt.TextFlag.TextSingleLine, model_index.data().split("\n", 1)[0])
        width = size.width()

        for ref in ref_list:
            ref_size = style_option.fontMetrics.size(Qt.TextFlag.TextSingleLine, ref)
            width += ref_size.width() + (2 * self.HORIZONTAL_PADDING) + self.REF_GAP

        size.setWidth(int(width))
        return size

    def helpEvent(self, event, view, style_option, model_index):
        """Display the tooltip for a log message if it is multi-line, or too wide for the column"""

        display_tooltip = False

        if ((event.type() != QEvent.Type.ToolTip) or (not model_index.isValid()) or
                (model_index.data(Qt.ItemDataRole.ToolTipRole) is None)):
            display_tooltip = True
        elif "\n" in model_index.data(Qt.ItemDataRole.ToolTipRole):
            # Display the tooltip if it contains multiple lines
            display_tooltip = True
        else:
            # Display the tooltip if the message doesn't fit into the column
            size = self.sizeHint(style_option, model_index)
            rect = view.visualRect(model_index)

            if size.width() > rect.width():
                display_tooltip = True

        if display_tooltip:
            return super().helpEvent(event, view, style_option, model_index)

        return False
