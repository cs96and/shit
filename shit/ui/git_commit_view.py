#! /usr/bin/python3
# git_log_view.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Git commit view widget"""

import pygit2

from PyQt6.QtCore import Qt, QModelIndex, pyqtSlot
from PyQt6.QtGui import QCursor
from PyQt6.QtWidgets import QApplication

from .tight_views import TightTreeView
from ..models.git_commit_model import GitCommitModel

__all__ = ["GitCommitView"]


class GitCommitView(TightTreeView):
    """Git commit view widget"""

    def __init__(self, repo, path, icon_provider, stretched_column, *args, **kwargs):
        super().__init__(stretched_column=stretched_column, *args, **kwargs)
        self.repo = repo
        self.path = path
        self.icon_provider = icon_provider
        self.branch_name = None
        # TODO: self.setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu)
        # TODO: self.customContextMenuRequested.connect(self.on_commit_context_menu)

    def populate(self, commit=None, prev_commit=None):
        """Populate the view with the list of changed files from a commit"""
        if commit is None and prev_commit is None:
            # Diff working dir to HEAD of current branch
            prev_commit = self.repo.revparse_single(self.branch_name).peel(pygit2.GIT_OBJ_COMMIT)

        QApplication.instance().setOverrideCursor(QCursor(Qt.CursorShape.WaitCursor))
        self.setUpdatesEnabled(False)

        try:
            model = GitCommitModel(self.repo, self.path, self.icon_provider, commit, prev_commit)
            self.setModel(model)

            if (model.rowCount() > 0) and model.item(0, 0).hasChildren():
                for row in range(0, model.rowCount()):
                    self.setFirstColumnSpanned(row, QModelIndex(), True)

            self.expandAll()

        finally:
            self.setUpdatesEnabled(True)
            QApplication.instance().restoreOverrideCursor()

    @pyqtSlot(str)
    def on_branch_changed(self, branch_name):
        """Handle the current branch changing"""
        self.branch_name = branch_name
