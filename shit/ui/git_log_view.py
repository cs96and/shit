#! /usr/bin/python3
# git_log_view.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Git log view widget"""

import pygit2

from PyQt6.QtCore import Qt, QItemSelection, QSortFilterProxyModel, pyqtSignal, pyqtSlot
from PyQt6.QtGui import QCursor
from PyQt6.QtWidgets import QApplication

from .branch_delegate import BranchDelegate
from .message_delegate import MessageDelegate
from .tight_views import TightTableView
from ..background_task import create_background_task
from ..models.git_log_model import GitLogModel

__all__ = ["GitLogView"]


class GitLogView(TightTableView):
    """Git log view widget"""

    populate_finished = pyqtSignal(str)
    commits_selected = pyqtSignal(object, object)
    invalid_selection = pyqtSignal()

    def __init__(self, repo, path, stretched_column, *args, **kwargs):
        super().__init__(stretched_column=stretched_column, *args, **kwargs)
        self.repo = repo
        self.path = path
        self.populate_task = None
        self.__branch_to_get = None

        self.setModel(QSortFilterProxyModel(filterCaseSensitivity=Qt.CaseSensitivity.CaseInsensitive, filterKeyColumn=-1))

        self.selectionModel().selectionChanged.connect(self.on_selection_changed)

        # QAbstractItemView does not take ownership of the delegates, so have to store them to keep them alive
        self.column_delegates = (BranchDelegate(), MessageDelegate(self.repo))
        self.setItemDelegateForColumn(0, self.column_delegates[0])
        self.setItemDelegateForColumn(1, self.column_delegates[1])

        # TODO: self.setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu)
        # TODO: self.customContextMenuRequested.connect(self.on_log_context_menu)

    def populate(self):
        """Populate the log table in a background thread"""
        # Only start a worker thread if there is not already one running
        if self.populate_task is None or self.populate_task.isFinished():
            QApplication.instance().setOverrideCursor(QCursor(Qt.CursorShape.WaitCursor))
            self.populate_task = create_background_task(lambda: GitLogModel(self.repo, self.__branch_to_get, self.path),
                                                        self.__on_populate_finished)

    def refresh(self):
        """Refresh the current view"""
        self.column_delegates[1].refresh_refs()
        self.populate()

    def branch_name(self):
        """Get the name of the branch currently being displayed"""
        return self.model().sourceModel().branch_name

    def selected_commit(self):
        """Return the commit object for the currently selected row
           Returns None if multiple rows are selected"""
        selected_indexes = self.selectionModel().selectedRows(0)

        if len(selected_indexes) != 1:
            return None

        return self.__get_commit_from_model_index(selected_indexes[0])

    @pyqtSlot(QItemSelection, QItemSelection)
    def on_selection_changed(self, _selected, _deselected):
        """Handle rows being selected in the table"""
        selected_indexes = self.sender().selectedRows(0)

        if len(selected_indexes) == 1:
            commit = self.__get_commit_from_model_index(selected_indexes[0])
            self.commits_selected.emit(commit, None)

        elif len(selected_indexes) == 2:
            new_index, old_index = selected_indexes
            if new_index.row() > old_index.row():
                new_index, old_index = old_index, new_index

            new_commit = self.__get_commit_from_model_index(new_index)
            old_commit = self.__get_commit_from_model_index(old_index)

            self.commits_selected.emit(new_commit, old_commit)

        else:
            self.invalid_selection.emit()

    @pyqtSlot(str)
    def on_branch_changed(self, branch_name):
        """Handle the current branch changing"""
        self.__branch_to_get = branch_name
        self.populate()

    def __get_commit_from_model_index(self, model_index):
        """Get a commit object from a model index"""
        commit_id = model_index.data(Qt.ItemDataRole.UserRole)

        if commit_id != pygit2.Oid(b''):
            commit = self.repo[commit_id].peel(pygit2.GIT_OBJ_COMMIT)
        else:
            commit = None

        return commit

    def __on_populate_finished(self, git_log_model):
        """Handle background populate finishing"""
        self.setUpdatesEnabled(False)

        try:
            self.model().setSourceModel(git_log_model)
            self.make_stretched_column_resizable()
        finally:
            self.setUpdatesEnabled(True)
            QApplication.instance().restoreOverrideCursor()

        # Start again if the chosen branch has changed
        if self.__branch_to_get != git_log_model.branch_name:
            self.populate()
        else:
            self.populate_task = None
            self.populate_finished.emit(git_log_model.branch_name)
