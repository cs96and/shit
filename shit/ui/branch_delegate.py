#! /usr/bin/python3
# branch_delegate.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""QT Delegate for drawing the git branch diagram"""

from PyQt6.QtCore import Qt, QPointF, QSize
from PyQt6.QtGui import QBrush, QColor, QPalette, QPen, QPainter, QPainterPath
from PyQt6.QtWidgets import QStyledItemDelegate


class BranchDelegate(QStyledItemDelegate):
    """Delegate to draw the branch diagram in the first column of the log table"""
    BRANCH_WIDTH = 12
    BRANCH_COLOURS = (None, QColor("#ee4035"), QColor("#f37736"), QColor("#fdf470"), QColor("#00b159"), QColor("#0392cf"),
                      QColor("#ff77aa"))

    def paint(self, painter, style_option, model_index):
        """Draw the branch diagram for a single cell in the log table"""
        self.initStyleOption(style_option, model_index)

        data_tuple = model_index.data(Qt.ItemDataRole.UserRole + 1)

        if data_tuple is None:
            return

        child_branch_list, parent_branch_list, inactive_branch_list = data_tuple

        my_branch = child_branch_list[0] if child_branch_list else 0

        # Calculate the centre of the dot that will be drawn for the current commit
        rect = style_option.rect
        centre = QPointF(rect.x() + ((my_branch + 1) * self.BRANCH_WIDTH),
                         rect.y() + (rect.height() / 2))

        pen = QPen(Qt.PenStyle.SolidLine)
        pen.setJoinStyle(Qt.PenJoinStyle.RoundJoin)
        pen.setWidth(2)

        painter.setBrush(QBrush())
        painter.setRenderHint(QPainter.RenderHint.Antialiasing)

        def __get_branch_colour(branch):
            # Get branch color from BRANCH_COLOURS tuple.  If None, then use the default colour for the widget
            return self.BRANCH_COLOURS[branch % len(self.BRANCH_COLOURS)] or style_option.palette.color(QPalette.ColorRole.Text)

        # Draw a line for every branch that isn't part of this commit
        for branch in inactive_branch_list:
            pen.setColor(__get_branch_colour(branch))
            painter.setPen(pen)
            x = rect.x() + ((branch + 1) * self.BRANCH_WIDTH)
            painter.drawLine(QPointF(x, rect.y()), QPointF(x, rect.y() + rect.height()))

        # Draw lines up to the child commits
        for branch in child_branch_list:
            dest = QPointF(rect.x() + ((branch + 1) * self.BRANCH_WIDTH), rect.y())
            pen.setColor(__get_branch_colour(branch))
            painter.setPen(pen)
            if centre.x() == dest.x():
                painter.drawLine(centre, dest)
            else:
                path = QPainterPath(centre)
                path.quadTo(QPointF(dest.x(), centre.y()), dest)
                painter.drawPath(path)

        # Draw lines down to the parent commits
        for branch in parent_branch_list:
            dest = QPointF(rect.x() + ((branch + 1) * self.BRANCH_WIDTH), rect.y() + rect.height())
            pen.setColor(__get_branch_colour(branch))
            painter.setPen(pen)
            if centre.x() == dest.x():
                painter.drawLine(centre, dest)
            else:
                path = QPainterPath(centre)
                path.quadTo(QPointF(dest.x(), centre.y()), dest)
                painter.drawPath(path)

        # Draw the dot for the branch that the commit is on (filled for branches/merges)
        if len(parent_branch_list) > 1:
            first_parent_branch = next((b for b in parent_branch_list if b != my_branch), my_branch)
            background_colour = __get_branch_colour(first_parent_branch)
        elif len(child_branch_list) > 1:
            first_child_branch = next((b for b in child_branch_list if b != my_branch), my_branch)
            background_colour = __get_branch_colour(first_child_branch)
        else:
            background_colour = style_option.palette.color(QPalette.ColorRole.Base)

        painter.setBrush(QBrush(background_colour, Qt.BrushStyle.SolidPattern))
        pen.setColor(__get_branch_colour(my_branch))
        painter.setPen(pen)
        painter.drawEllipse(centre, 4., 4.)

    def sizeHint(self, style_option, model_index):
        """Calculate the size of the branch diagram"""
        self.initStyleOption(style_option, model_index)

        data_tuple = model_index.data(Qt.ItemDataRole.UserRole + 1)

        if data_tuple is None:
            return QSize(0, style_option.rect.height())

        child_branch_list, parent_branch_list, inactive_branch_list = data_tuple

        max_child_branch = child_branch_list[-1] if child_branch_list else 0
        max_parent_branch = parent_branch_list[-1] if parent_branch_list else 0
        max_inactive_branch = max(inactive_branch_list) if inactive_branch_list else 0
        max_branch = max(max_child_branch, max_parent_branch, max_inactive_branch)

        return QSize((max_branch + 2) * self.BRANCH_WIDTH, style_option.rect.height())
