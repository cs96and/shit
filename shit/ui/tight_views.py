#! /usr/bin/python3
# tight_views.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""QT views with less whitespace"""

__all__ = ["TightTableView", "TightTreeView"]

from PyQt6.QtCore import Qt, QModelIndex, QPersistentModelIndex, QTimer, pyqtSlot
from PyQt6.QtWidgets import QTableView, QTreeView
from PyQt6.QtWidgets import QHeaderView, QProxyStyle


class HeaderProxyStyle(QProxyStyle):
    """Proxy style to reduce the amount of padding in headers"""

    def pixelMetric(self, metric, style_option, widget):
        """Set the header margin to 3"""
        if metric == QProxyStyle.PixelMetric.PM_HeaderMargin:
            return 3

        return super().pixelMetric(metric, style_option, widget)


def _tight_view_factory(cls):
    """Generate a TightView class, derived from the provided base class"""
    class TightView(cls):
        """View class with less whitespace between rows, no vertical header, no grid and row-selection enabled"""
        def __init__(self, stretched_column, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.stretched_column = stretched_column

            try:
                self.verticalHeader().setDefaultSectionSize(self.fontMetrics().lineSpacing() + 5)
                self.verticalHeader().hide()
            except AttributeError:
                pass

            try:
                self.horizontal_header = self.horizontalHeader()
            except AttributeError:
                try:
                    self.horizontal_header = self.header()
                except AttributeError:
                    self.horizontal_header = None

            if self.horizontal_header is not None:
                self.horizontal_header.setStyle(HeaderProxyStyle())
                self.horizontal_header.setDefaultAlignment(Qt.AlignmentFlag.AlignLeft)
                self.horizontal_header.setStretchLastSection(False)
                self.horizontal_header.setResizeContentsPrecision(10000)

            try:
                self.setUniformRowHeights(True)
            except AttributeError:
                pass

            try:
                self.setShowGrid(False)
            except AttributeError:
                pass

            try:
                self.setIndentation(0)
            except AttributeError:
                pass

            self.setSelectionBehavior(self.SelectionBehavior.SelectRows)
            self.setSelectionMode(self.SelectionMode.ExtendedSelection)
            self.scroll_timer = QTimer(singleShot=True, interval=0, timeout=self.on_scroll_timeout)
            self.scroll_to_persistent_index = None

        def setModel(self, *args, **kwargs):
            """Set the model for the view and set up signal/slot connections"""
            if self.selectionModel():
                self.selectionModel().deleteLater()

            super().setModel(*args, **kwargs)
            self.model().rowsInserted.connect(self.on_rows_inserted_or_removed)
            self.model().rowsRemoved.connect(self.on_rows_inserted_or_removed)

        def make_stretched_column_resizable(self):
            """Stretch a column to fill the width, but also make it resizable"""

            # A stretched column disables resizing, but we want it to be resizable.
            if self.stretched_column < self.horizontal_header.count():
                # First call resizeColumnsToContents() to make the stretched column the size that we want.
                self.horizontal_header.setSectionResizeMode(self.stretched_column, QHeaderView.ResizeMode.Stretch)
                for column in range(self.horizontal_header.count()):
                    self.resizeColumnToContents(column)

                # Calculate the current width of the columns, minus the scroll bar size.
                width = self.horizontal_header.size().width() - self.verticalScrollBar().sizeHint().width()

                # Set the message column to interactive
                self.horizontal_header.setSectionResizeMode(self.stretched_column, QHeaderView.ResizeMode.Interactive)

                # Calculate the required size of the message column by subtracting the size of the other columns
                for column in range(self.horizontal_header.count()):
                    if column != self.stretched_column:
                        width -= self.horizontal_header.sectionSize(column)

                # Set the column width
                self.setColumnWidth(self.stretched_column, width)

        def rowsAboutToBeRemoved(self, _model_index, start, end):
            """Handle rows being removed.  Clear currently selected item if its row is being removed"""
            current_index = self.selectionModel().currentIndex()
            if current_index.isValid():
                if start <= current_index.row() <= end:
                    self.selectionModel().clearCurrentIndex()

        @pyqtSlot(QModelIndex, int, int)
        def on_rows_inserted_or_removed(self, _model_index, _start, _end):
            """Handle rows being inserted/removed.  Ensure the selected item is still visible"""
            current_index = self.selectionModel().currentIndex()
            if current_index.isValid():
                # Calling scrollTo() immediately doesn't seem to work.  Also, this function gets called a ton of times in a row.
                # Scroll to the selected item next time round the event loop.
                # If more items get inserted or removed in the meantime, only one scroll will take place.
                self.scroll_to_persistent_index = QPersistentModelIndex(current_index)
                self.scroll_timer.start()

        @pyqtSlot()
        def on_scroll_timeout(self):
            """Handle the scroll timer finishing.  Actually do the scrolling if the index is still valid"""
            if self.scroll_to_persistent_index.isValid():
                self.scrollTo(QModelIndex(self.scroll_to_persistent_index), self.ScrollHint.PositionAtCenter)
                self.scroll_to_persistent_index = None

    return TightView


TightTableView = _tight_view_factory(QTableView)
TightTreeView = _tight_view_factory(QTreeView)
