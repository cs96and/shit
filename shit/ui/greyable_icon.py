#! /usr/bin/python3
# greyable_icon.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Icon with normal and 'greyed out' versions"""

from PyQt6.QtGui import QIcon


class GreyableIcon(QIcon):
    """Icon that can be switched between normal and grey versions"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.grey_icon = QIcon()
        for size in self.availableSizes():
            pixmap = self.pixmap(size, QIcon.Mode.Disabled)
            self.grey_icon.addPixmap(pixmap)

    def enabled(self):
        """Return the 'enabled' version of the icon"""
        return self

    def disabled(self):
        """Return the 'disabled' (grey) version of the icon"""
        return self.grey_icon
