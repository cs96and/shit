#! /usr/bin/python3
# __init__.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Shit package initialization"""

import os
import signal
import sys

from .shit_app import ShitApp
from .settings import Settings

ICONS_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "icons")
SETTINGS = Settings()


def main():
    """Run the shit application"""

    # Use the default OS Ctrl+C handler
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    exit_code = 0
    with ShitApp(sys.argv) as app:
        exit_code = app.exec()
    sys.exit(exit_code)
