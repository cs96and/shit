#! /usr/bin/python3
# detachable_process.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Child process that can be detatched so that it does not exit when shit exits"""

from PyQt6.QtCore import QProcess


class DetachableProcess(QProcess):
    """Add detach() method to QProcess"""

    def detach(self):
        """Detach the child process from the parent"""
        self.waitForStarted()
        self.setProcessState(QProcess.ProcessState.NotRunning)
