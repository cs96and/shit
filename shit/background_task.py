#! /usr/bin/python3
# background_task.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Helper function to run a function in a background thread"""

from PyQt6.QtCore import QThread, pyqtSlot


class _BackgroundTask(QThread):
    """Thread class to run a function in the background"""

    def __init__(self, function):
        super().__init__()
        self.function = function
        self.result = None

    def run(self):
        """Run the function in a background thread"""
        try:
            self.result = self.function()
        except BaseException as error:
            self.result = error


def create_background_task(function, callback=None, error_callback=None):
    """Run a function in a background thread.
       If function returns a value, this will be passed a single parameter to the callback function.
       If function returns a tuple of values, these will be expanded and passed as individual parameters to the callback function.
       error_callback should take a single parameter which will be the triggering exception"""

    @pyqtSlot()
    def on_finished():
        if isinstance(task.result, BaseException):
            if error_callback is not None:
                error_callback(task.result)
        else:
            if callback is not None:
                if task.result is None:
                    callback()
                elif isinstance(task.result, tuple):
                    callback(*task.result)
                else:
                    callback(task.result)

    task = _BackgroundTask(function)
    task.finished.connect(on_finished)
    task.start()
    return task
