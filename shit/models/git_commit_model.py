#! /usr/bin/python3
# git_commit_model.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""QT Model for holding informatin about a git commit"""

import os
import tempfile

import pygit2
from PyQt6.QtCore import Qt, QFileInfo, QMimeDatabase
from PyQt6.QtGui import QColor, QIcon, QStandardItemModel

from .model_item import ModelItem, TreeModelItem

__all__ = ["GitCommitModel"]

DELTA_STATUS_TO_STRING_MAP = {
    pygit2.GIT_DELTA_ADDED: "Added",
    pygit2.GIT_DELTA_COPIED: "Copied",
    pygit2.GIT_DELTA_DELETED: "Deleted",
    pygit2.GIT_DELTA_IGNORED: "Ignored",
    pygit2.GIT_DELTA_MODIFIED: "Modified",
    pygit2.GIT_DELTA_RENAMED: "Renamed",
    pygit2.GIT_DELTA_TYPECHANGE: "TypeChange",
    pygit2.GIT_DELTA_UNMODIFIED: "Unmodified",
    pygit2.GIT_DELTA_UNREADABLE: "Unreadable",
    pygit2.GIT_DELTA_UNTRACKED: "Untracked"
}

DELTA_STATUS_TO_COLOUR_MAP = {
    pygit2.GIT_DELTA_ADDED: Qt.GlobalColor.darkGreen,
    pygit2.GIT_DELTA_COPIED: Qt.GlobalColor.darkMagenta,
    pygit2.GIT_DELTA_DELETED: Qt.GlobalColor.darkRed,
    pygit2.GIT_DELTA_IGNORED: Qt.GlobalColor.darkGray,
    pygit2.GIT_DELTA_RENAMED: Qt.GlobalColor.blue,
    pygit2.GIT_DELTA_UNTRACKED: Qt.GlobalColor.darkGray
}


def delta_status_to_string(delta_status):
    """Convert a GIT_DELTA_* enum to a string"""
    return DELTA_STATUS_TO_STRING_MAP.get(delta_status, "Unknown")


def get_delta_status_colour(delta_status):
    """Get the text colour for a given GIT_DELTA_*"""
    colour = DELTA_STATUS_TO_COLOUR_MAP.get(delta_status, None)
    return QColor(colour) if colour else None


class ChangesBase:
    """Common base class for WorkingChanges and StagedChanges.  Can't derive from pygit2.Oid for some reason"""
    def __init__(self, oid):
        self.oid = oid


class WorkingChanges(ChangesBase):
    """Wrapper class for indicating a diff is for working directory changes"""
    pass


class StagedChanges(ChangesBase):
    """Wrapper class for indicating a diff is for staged changes"""
    pass


class GitCommitModel(QStandardItemModel):
    """Model for storing git commit info"""

    def __init__(self, repo, path, icon_provider, commit=None, prev_commit=None):
        super().__init__(0, 5)
        self.repo = repo
        self.path = path
        self.icon_provider = icon_provider
        self.setHorizontalHeaderLabels(["", "Path", "Status", "LinesAdded", "LinesRemoved"])

        if prev_commit is None:
            if commit.parents:
                # Diff a single commit to its parents
                diffs = [(parent.id, parent.tree.diff_to_tree(commit.tree)) for parent in commit.parents]
            else:
                # No parents, diff against empty tree
                diffs = [(None, commit.tree.diff_to_tree(swap=True))]

        elif commit is None:
            # Ensure index is up to date before doing any diffs
            repo.index.read()

            if prev_commit.id == self.repo.head.target:
                # Diff staged changes
                staged_diff = prev_commit.tree.diff_to_index(repo.index)
                diffs = []
                if staged_diff:
                    diffs.append((StagedChanges(prev_commit.id), staged_diff))

                # Diff HEAD to working dir
                working_diff = repo.index.diff_to_workdir(pygit2.GIT_DIFF_INCLUDE_UNTRACKED)
                if working_diff:
                    diffs.append((WorkingChanges(repo.head.target), working_diff))
            else:
                # Diff non-HEAD to workdir
                index_diff = prev_commit.tree.diff_to_index(repo.index)
                working_diff = repo.index.diff_to_workdir(pygit2.GIT_DIFF_INCLUDE_UNTRACKED)
                index_diff.merge(working_diff)
                diffs = [(WorkingChanges(prev_commit.id), index_diff)]

        else:
            # Diff between two commits
            diffs = [(prev_commit.id, prev_commit.tree.diff_to_tree(commit.tree))]

        self.__populate(diffs, commit.id if commit else None)

    def remove_all_rows(self):
        """Remove all rows from the model (but leave the header)"""
        self.removeRows(0, self.rowCount())

    def __populate(self, diffs, commit_id):
        """Populate the model with the list of files that were modified in the commit"""

        for i, (parent_id, diff) in enumerate(diffs, 1):
            if isinstance(parent_id, WorkingChanges):
                parent_item = TreeModelItem("Working directory changes")
                parent_id = parent_id.oid
            elif isinstance(parent_id, StagedChanges):
                parent_item = TreeModelItem("Staged changes")
                parent_id = parent_id.oid
            elif len(diffs) > 1:
                parent_item = TreeModelItem(f"Diff to parent {i} ({parent_id})")

            try:
                self.invisibleRootItem().appendRow(parent_item)
            except NameError:
                parent_item = self

            diff.find_similar(pygit2.GIT_DIFF_FIND_RENAMES | pygit2.GIT_DIFF_FIND_COPIES)
            for patch in diff:
                patch_path = patch.delta.new_file.path

                if (self.path == '.') or patch_path.startswith(self.path):
                    text_colour = get_delta_status_colour(patch.delta.status)
                else:
                    text_colour = Qt.GlobalColor.darkGray

                if patch.delta.status in [pygit2.GIT_DELTA_RENAMED, pygit2.GIT_DELTA_COPIED]:
                    patch_path += f" (from {patch.delta.old_file.path})"

                parent_item.appendRow((
                    ModelItem(self.__get_file_icon(patch_path), "",
                              data={'patch': patch, 'old_id': parent_id, 'new_id': commit_id}),
                    ModelItem(patch_path, tooltip=patch_path, colour=text_colour),
                    ModelItem(delta_status_to_string(patch.delta.status), colour=text_colour),
                    ModelItem(str(patch.line_stats[1]), colour=text_colour),
                    ModelItem(str(patch.line_stats[2]), colour=text_colour)
                ))

    def __get_file_icon(self, filename):
        """Get the icon for a filename"""
        if self.icon_provider:
            # Hack the path to something non-existent to prevent icon overlays appearing
            file_info = QFileInfo(os.path.join(tempfile.mktemp(), filename))
            icon = self.icon_provider.icon(file_info)
        else:
            mime_type = QMimeDatabase().mimeTypeForFile(filename, QMimeDatabase.MatchMode.MatchExtension)
            icon = QIcon.fromTheme(mime_type.iconName())

        return icon
