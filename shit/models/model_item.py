#! /usr/bin/python3
# model_item.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Helper classes for creating QT model items for table and tree views"""

from PyQt6.QtCore import Qt
from PyQt6.QtGui import QStandardItem


class TreeModelItem(QStandardItem):
    """Helper class to create QStandardItems for trees, with optional user data, tooltip and colour"""

    def __init__(self, *args, data=None, data2=None, tooltip=None, colour=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.setFlags(Qt.ItemFlag.ItemIsEnabled | Qt.ItemFlag.ItemIsSelectable)
        if data:
            self.setData(data, Qt.ItemDataRole.UserRole)
        if data2:
            self.setData(data2, Qt.ItemDataRole.UserRole + 1)
        if tooltip:
            self.setToolTip(tooltip)
        if colour:
            self.setForeground(colour)


class ModelItem(TreeModelItem):
    """Helper class to create QStandardItems for tables, with optional user data, tooltip and colour"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setFlags(self.flags() | Qt.ItemFlag.ItemNeverHasChildren)
