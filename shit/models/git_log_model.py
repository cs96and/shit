#! /usr/bin/python3
# git_log_model.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""QT Model for holding information about a git log"""

import bisect
import os
from datetime import datetime

import pygit2
from PyQt6.QtGui import QStandardItemModel

from .model_item import ModelItem


class GitLogModel(QStandardItemModel):
    """Model for storing git log info"""
    def __init__(self, repo, branch_name, path):
        super().__init__(0, 4)
        self.repo = repo
        self.branch_name = branch_name
        if os.name == 'nt':
            self.path = path.replace('\\', '/')
        else:
            self.path = path
        self.setHorizontalHeaderLabels(["Branch", "Message", "Author", "Date"])
        self.__populate()

    def __populate(self):
        """Populate the git log model"""

        # Append special row for working dir changes
        self.appendRow((
            ModelItem(data=pygit2.Oid(b'')),
            ModelItem("Working directory changes")
        ))

        is_root = (self.path == '.')
        branch_map = {}

        # Walk the commits for the checked out branch
        for commit in self.repo.walk(self.repo.revparse_single(self.branch_name).id, pygit2.GIT_SORT_TOPOLOGICAL):
            # Get child branch info for the commit
            child_branch_list = branch_map.pop(commit.id, [])
            inactive_branch_set = self.__get_branch_num_set(branch_map)
            my_branch = child_branch_list[0] if child_branch_list else self.__get_next_branch_num(inactive_branch_set)

            parent_branch_list = []
            if commit.parents:
                bisect.insort(parent_branch_list, my_branch)
                self.__update_branch_map(branch_map, commit.parents[0].id, my_branch)
                for parent in commit.parents[1:]:
                    next_branch_list = branch_map.get(parent.id, None)
                    if not next_branch_list:
                        next_branch_list = [self.__get_next_branch_num(branch_map)]
                        self.__update_branch_map(branch_map, parent.id, next_branch_list[0])

                    bisect.insort(parent_branch_list, next_branch_list[0])

            found_parent = changed = False

            if is_root:
                # If we are not filtering to a subdir, allow everything through
                changed = found_parent = True
            else:
                # We are filtering to a subdir or file.  Get the tree id of the subdir.
                try:
                    tree_id = commit.tree[self.path].id
                except KeyError:
                    continue

                for parent in commit.parents:
                    try:
                        # Get the tree id of the subdir in the parent commit
                        parent_id = parent.tree[self.path].id
                    except KeyError:
                        pass
                    else:
                        # Check if the tree id has changed
                        found_parent = True
                        if parent_id != tree_id:
                            changed = True
                            break

            if changed or not found_parent:
                stripped_message = commit.message.rstrip("\n")
                self.appendRow((
                    ModelItem(commit.id.hex, data=commit.id,
                              data2=(child_branch_list, parent_branch_list, (inactive_branch_set))),
                    ModelItem(stripped_message, tooltip=stripped_message),
                    ModelItem(commit.author.name, tooltip=commit.author.email),
                    ModelItem(str(datetime.fromtimestamp(commit.commit_time)))
                ))

            if not found_parent:
                break

    @staticmethod
    def __get_branch_num_set(branch_map):
        """Get a set of all active branches"""
        branch_num_set = set()
        for branch_list in branch_map.values():
            branch_num_set.update(branch_list)
        return branch_num_set

    @staticmethod
    def __get_next_branch_num(branches):
        """Calculate the next available branch number"""
        branch_num_set = branches if isinstance(branches, set) else GitLogModel.__get_branch_num_set(branches)

        branch_num = 0
        while True:
            if branch_num not in branch_num_set:
                break
            else:
                branch_num += 1

        return branch_num

    @staticmethod
    def __update_branch_map(branch_map, commit_id, branch_num):
        """Update the branch numbers for a given commit.
           If the commit already exists in the map, add the branch to the list"""
        # Find the branch list for the commit, or add a new blank list
        branch_list = branch_map.setdefault(commit_id, [])
        # Add the new branch to the list
        bisect.insort(branch_list, branch_num)
