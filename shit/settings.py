#! /usr/bin/python3
# settings.py
# -*- coding: utf-8 -*-

# Copyright (C) 2018-2023 Alan Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Module to hold settings for Shit"""

__all__ = ["Settings"]

import os
import configparser

if os.name == 'nt':
    DIFF_APP = "C:\\Program Files\\WinMerge2011\\WinMergeU.exe"
else:
    DIFF_APP = "diffuse"

DEFAULTS = {
    "diff": DIFF_APP,
    "diffargs": "{left} {right}",
    "logfont": None,
    "logfontsize": None
}

TYPES = {
    "logfontsize": float
}


class Settings:
    """Class to hold Shit settings and read them from a file"""

    def __init__(self):
        if os.name == 'nt':
            ini_dir = os.path.join(os.environ["APPDATA"], "Shit")
            if not os.path.exists(ini_dir):
                os.mkdir(ini_dir)

            ini_path = os.path.join(ini_dir, "shit.ini")
        else:
            ini_path = os.path.join(os.environ["HOME"], ".shitrc")

        self.config = configparser.ConfigParser(defaults=DEFAULTS, default_section="shit", allow_no_value=True)

        if not self.config.read(ini_path):
            with open(ini_path, 'w') as f:
                self.config.write(f)

    def __getitem__(self, key):
        """Get a setting"""
        value = self.config["shit"][key]
        if value:
            try:
                # Attempt to convert the setting if needed
                value_type = TYPES[key]
                return value_type(value)

            except KeyError:
                pass

        return value
