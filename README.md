# Shit - Git log viewer

## REPOSITORY MOVED
This repository is now available at https://gitlab.com/cs96and/Shit

The code here may be out of date

## Introduction

Shit is a git log viewer written in Python and PyQT.  Unable to find a decent git log viewer for Linux, I decided
to write my own.  The main feature I needed was the ability to view the log of a sub-directory of the repository.

This is still very much a work in progress, so functionality is currently limited and bugs may be present.

## Licence

Shit is licensed under the GPLv3 license. Please
see the file LICENSE.txt for full information on the licence.

Shit contains icons from [EmojiOne](https://www.emojione.com) / [JoyPixels](http://joypixels.com)
